createGrid = function() {
// add a wrapper to the dom
	var gridSize = ( 32 * 32 );
	var gridWrapper = 'pixelWrap';
	document.getElementById( 'rec_time' ).insertAdjacentHTML( 'beforeend', '<div id="' + gridWrapper + '"></div>' );
// start a loop to generate second marks on the watchface.
	for( gridLoop = 1; gridLoop <= gridSize ; gridLoop++ ) {
		// define variables
			gridLoopClass = 'gridItem'
			gridLoopWrapper = document.getElementById(gridWrapper);
			gridLoopId = gridLoopClass + gridLoop ;
			gridLoopMarkup = '<div id="' + gridLoopId + '" class="' + gridLoopClass + '"></div>';
		// define functions
			gridLoopInsert = function() {
				gridLoopWrapper.insertAdjacentHTML( 'beforeend', gridLoopMarkup );
			};
		// run stuff
			gridLoopInsert();
	}
};
createGrid();
