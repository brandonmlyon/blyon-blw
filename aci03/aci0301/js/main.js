/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var flagConsole = false,
battery = navigator.battery || navigator.webkitBattery || navigator.mozBattery,
interval;

function displayWeekDay(date) {
	var str_day = document.getElementById('str_day'),
	get_day = date.getDay(),
	str_allday;
	arr_day = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
	arr_month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec" ],
	get_date = date.getDate(),
	month = date.getMonth();

	if (get_date < 10) {
		get_date = "0" + get_date;
	}

	str_allday = arr_day[get_day] + " " + get_date + " " + arr_month[month];
	str_day.innerHTML = str_allday;
}

function displayTime() {

	try{
		date  = tizen.time.getCurrentDateTime();
	}catch(e) {
		alert(e.message);
	}

	displayWeekDay(date);

	hours = date.getHours();
	minutes = date.getMinutes();
	seconds = date.getSeconds();
	if (hours < 12) {
		hourAngle = ( 360 /  12 ) * hours;
	}	else {
		hourAngle = ( 360 /  12 ) * ( hours - 12 );
	}
	minuteAngle = ( 360 / 60 ) * minutes;
	secondAngle = ( 360 / 60 ) * seconds;
	hourHand = document.getElementById('hourCircleAngle');
		hourHandHalf= document.getElementById('hourCircleAngleHalf');
		hourHandOverflow = document.getElementById('hourCircleOverflow');
	minuteHand = document.getElementById('minuteCircleAngle');
		minuteHandHalf = document.getElementById('minuteCircleAngleHalf');
		minuteHandOverflow = document.getElementById('minuteCircleOverflow');
	secondHand = document.getElementById('secondCircleAngle');
		secondHandHalf = document.getElementById('secondCircleAngleHalf');
		secondHandOverflow = document.getElementById('secondCircleOverflow');
	rotateDegHours = 'rotate(' + hourAngle + 'deg)';
	rotateDegMinutes = 'rotate(' + minuteAngle + 'deg)';
	rotateDegSeconds = 'rotate(' + secondAngle + 'deg)';

	if (hourAngle >= 180 ) {
		hourHandHalf.style.visibility = 'visible';
		hourHandOverflow.style.overflow = 'visible';
	} else {
		hourHandHalf.style.visibility = 'hidden';
		hourHandOverflow.style.overflow = 'hidden';
	}
	if (minuteAngle >= 180 ) {
		minuteHandHalf.style.visibility = 'visible';
		minuteHandOverflow.style.overflow = 'visible';
	} else {
		minuteHandHalf.style.visibility = 'hidden';
		minuteHandOverflow.style.overflow = 'hidden';
	}
	if (secondAngle >= 180 ) {
		secondHandHalf.style.visibility = 'visible';
		secondHandOverflow.style.overflow = 'visible';
	} else {
		secondHandHalf.style.visibility = 'hidden';
		secondHandOverflow.style.overflow = 'hidden';
	}

	hourHand.style.transform = rotateDegHours;
	minuteHand.style.transform = rotateDegMinutes;
	secondHand.style.transform = rotateDegSeconds;

	// document.getElementById('consoleLog').innerHTML = hourAngle + ", " + minuteAngle + ", " + secondAngle;

	if (flagConsole) {
		str_console.style.visibility = 'visible';
		flagConsole = false;
	} else {
		str_console.style.visibility = 'hidden';
		flagConsole = true;
	}
}

function initDigitalWatch() {
	interval = setInterval(displayTime, 500);
}

function ambientDigitalWatch() {
	clearInterval(interval);
	displayTime();
	document.getElementById('str_console').style.visibility = 'visible';
}

function getBatteryState() {
	var battery_level = Math.floor(battery.level * 10),
	battery_fill = document.getElementById('battery_fill');

	battery_level = battery_level;
	battery_fill.style.width = battery_level * 6 + "%";

}

function bindEvents() {
	battery.addEventListener('chargingchange', getBatteryState);
	battery.addEventListener('chargingtimechange', getBatteryState);
	battery.addEventListener('dischargingtimechange', getBatteryState);
	battery.addEventListener('levelchange', getBatteryState);

	// add eventListener for timetick
	window.addEventListener('timetick', function() {
		ambientDigitalWatch();
	});

	// add eventListener for ambientmodechanged
	window.addEventListener('ambientmodechanged', function(e) {
		console.log("ambientmodechanged : " + e.detail.ambientMode);
		if (e.detail.ambientMode === true) {
			// rendering ambient mode case
			ambientDigitalWatch();

		} else {
			// rendering normal case
			initDigitalWatch();
		}
	});
}

window.onload = function() {
	document.addEventListener('tizenhwkey', function(e) {
		if (e.keyName === "back") {
			try {
				tizen.application.getCurrentApplication().exit();
			} catch (ignore) {}
		}
	});

	displayTime();
	initDigitalWatch();
	bindEvents();
};
