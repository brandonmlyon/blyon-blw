// Universal watchface javascript, based off of dnu02.
/*
Define how to generate the date.
Define how to generate the time, render it, render things related to it (ie the date).
Define a function to run those functions.
Define how often to update the time.
Define how to generate the battery data and render it.
Attach a listener to events.
Define what happens on hardware ready state
*/

/* TODO: Put a 12h/24h toggle in all modes */
/* LEFT OFF AT: integrating all required js. need secondMarks.js, anu01, pix32 */

/* WATCHFACE CONFIGURATION ===========================================================*/
  /* digital */
    var showTimeNumbers = true;
    var showTimeWords = false;
    var showDateWords = true;
    var showDateNumbers = false;
  /* analog */
    var showAnalogHands = false;
    var showAnalogHourMarks = false;
    var showAnalogSecondMarks = false;
    var showAnalogHourNumbers = false;
  /* fancy */
    var showTimeRadials = false;
    var showWatchGrid = false;
  /* other */
    var showBatteryLines = false;
    var showBatteryNumber = false;
    var showConsoleLog = false;

/* BEGIN JAVASCRIPT ==================================================================*/

var battery = navigator.battery || navigator.webkitBattery || navigator.mozBattery;
var interval = {};

function displayTimeAsNumbers(date)
{
  str_hours.innerHTML = hours;
  str_minutes.innerHTML = minutes;
  str_seconds.innerHTML = seconds;
  /* Prepend a zero to small time counts */
    if (hours < 10)
    {
        str_hours.innerHTML = "0" + date.getHours();
    }
    if (date.getMinutes() < 10)
    {
        str_minutes.innerHTML = "0" + date.getMinutes();
    }
    if (date.getMinutes() < 10)
    {
        str_seconds.innerHTML = "0" + date.getSeconds();
    }
}
function displayTimeAsWords(date)
{
  /* Numbers to words from http://codereview.stackexchange.com/a/90563 */
    var ONE_TO_NINETEEN = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
    var TENS = ["ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    var SCALES = ["thousand", "million", "billion", "trillion"];
    function isTruthy(item)
    {
      return !!item;
    }
    function chunk(number)
    {
      var thousands = [];
      while(number > 0)
      {
        thousands.push(number % 1000);
        number = Math.floor(number / 1000);
      }
      return thousands;
    }
    function inEnglish(number)
    {
      var thousands, hundreds, tens, ones, words = [];
      if(number < 20)
      {
        return ONE_TO_NINETEEN[number - 1];
      }
      if(number < 100)
      {
        ones = number % 10;
        tens = number / 10 | 0;
        words.push(TENS[tens - 1]);
        words.push(inEnglish(ones));
        return words.filter(isTruthy).join("-");
      }
      hundreds = number / 100 | 0;
      words.push(inEnglish(hundreds));
      words.push("hundred");
      words.push(inEnglish(number % 100));
      return words.filter(isTruthy).join(" ");
    }
    function appendScale(chunk, exp)
    {
      var scale;
      if(!chunk)
      {
        return null;
      }
      scale = SCALES[exp - 1];
      return [chunk, scale].filter(isTruthy).join(" ");
    }
  /* end "from stackexchange" */
  /* convert time to words */
    hours = date.getHours();
    if (hours == "0")
    {
      timeHours = "zero";
    }
    else
    {
      timeHours = inEnglish(hours);
    }
    str_hours.innerHTML = timeHours;
    minutes = date.getMinutes();
    if (minutes == "0")
    {
      timeMinutes = "zero";
    }
    else
    {
      timeMinutes = inEnglish(minutes);
    }
    str_minutes.innerHTML = timeMinutes;
}
function displayRadials(date)
{
  if (hours < 12)
  {
    hourAngle = ( 360 /  12 ) * hours;
  }
  else
  {
    hourAngle = ( 360 /  12 ) * ( hours - 12 );
  }
  minuteAngle = ( 360 / 60 ) * minutes;
  secondAngle = ( 360 / 60 ) * seconds;
  hourHand = document.getElementById('hourCircleAngle');
    hourHandHalf= document.getElementById('hourCircleAngleHalf');
    hourHandOverflow = document.getElementById('hourCircleOverflow');
  minuteHand = document.getElementById('minuteCircleAngle');
    minuteHandHalf = document.getElementById('minuteCircleAngleHalf');
    minuteHandOverflow = document.getElementById('minuteCircleOverflow');
  secondHand = document.getElementById('secondCircleAngle');
    secondHandHalf = document.getElementById('secondCircleAngleHalf');
    secondHandOverflow = document.getElementById('secondCircleOverflow');
  rotateDegHours = 'rotate(' + hourAngle + 'deg)';
  rotateDegMinutes = 'rotate(' + minuteAngle + 'deg)';
  rotateDegSeconds = 'rotate(' + secondAngle + 'deg)';
  if (hourAngle >= 180 )
  {
    hourHandHalf.style.visibility = 'visible';
    hourHandOverflow.style.overflow = 'visible';
  }
  else
  {
    hourHandHalf.style.visibility = 'hidden';
    hourHandOverflow.style.overflow = 'hidden';
  }
  if (minuteAngle >= 180 )
  {
    minuteHandHalf.style.visibility = 'visible';
    minuteHandOverflow.style.overflow = 'visible';
  }
  else
  {
    minuteHandHalf.style.visibility = 'hidden';
    minuteHandOverflow.style.overflow = 'hidden';
  }
  if (secondAngle >= 180 )
  {
    secondHandHalf.style.visibility = 'visible';
    secondHandOverflow.style.overflow = 'visible';
  }
  else
  {
    secondHandHalf.style.visibility = 'hidden';
    secondHandOverflow.style.overflow = 'hidden';
  }
  hourHand.style.transform = rotateDegHours;
  minuteHand.style.transform = rotateDegMinutes;
  secondHand.style.transform = rotateDegSeconds;
}
function displayAnalogHands(date)
{
  if (hours < 12)
  {
    hourAngle = ( 360 /  12 ) * hours;
  }
  else
  {
    hourAngle = ( 360 /  12 ) * ( hours - 12 );
  }
  minuteAngle = ( 360 / 60 ) * minutes;
  secondAngle = ( 360 / 60 ) * seconds;
  hourHand = document.getElementById('hourHand');
  minuteHand = document.getElementById('minuteHand');
  secondHand = document.getElementById('secondHand');
  rotateDegHours = 'rotate(' + hourAngle + 'deg)';
  rotateDegMinutes = 'rotate(' + minuteAngle + 'deg)';
  rotateDegSeconds = 'rotate(' + secondAngle + 'deg)';
  hourHand.style.transform = rotateDegHours;
  minuteHand.style.transform = rotateDegMinutes;
  secondHand.style.transform = rotateDegSeconds;
}
/* Display the calendar date in plain words */
function displayWeekDay(date)
{
  var str_day = document.getElementById('str_day');
  var get_day = date.getDay();
  var str_allday = '';
  var arr_day = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  var arr_month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec" ];
  var get_date = date.getDate();
  var month = date.getMonth();
  /* Prepend zeroes to small dates */
    if (get_date < 10)
    {
      get_date = "0" + get_date;
    }
  /* Assemble the date string */
    str_allday = arr_day[get_day] + " " + get_date + " " + arr_month[month];
  /* Insert the date into the HTML */
    str_day.innerHTML = str_allday;
}
/* Display things related to time */
function displayTime()
{
  var str_hours = document.getElementById('str_hours');
  var str_minutes = document.getElementById('str_minutes');
  var str_seconds = document.getElementById('str_seconds');
  var date = '';
  /* Set the date variable to the OS date */
    try
    {
        date  = tizen.time.getCurrentDateTime();
    }
    catch(e)
    {
        alert(e.message);
    }
  /* Render the calendar date */
    displayWeekDay(date);
  /* Modify the HTML */
    hours = date.getHours();
    minutes = date.getMinutes();
    seconds = date.getSeconds();
    if (showTimeNumbers = true){displayTimeAsNumbers(date);}
    if (showTimeWords = true){displayTimeAsWords(date);}
    if (showTimeRadials = true){displayRadials(date);}
    if (showAnalogHands = true){displayAnalogHands(date);}
  //document.getElementById('consoleLog').innerHTML = hours + ", " + minutes + ", " + seconds;
}
/* Initialize the watch  */
function initDigitalWatch()
{
  interval = setInterval(displayTime, 500);
}
/* Display the battery level */
function getBatteryState()
{
  var battery_level = Math.floor(battery.level * 10);
  var battery_fill = document.getElementById('battery_fill');
  /* Set the battery level */
    battery_level = battery_level;
  /* Insert the battery level into the HTML */
    battery_fill.style.width = battery_level * 6 + "%";
}
/* Attach a listener to run things when time changes */
function bindEvents()
{
  battery.addEventListener('chargingchange', getBatteryState);
  battery.addEventListener('chargingtimechange', getBatteryState);
  battery.addEventListener('dischargingtimechange', getBatteryState);
  battery.addEventListener('levelchange', getBatteryState);
}
/* Start it all */
window.onload = function()
{
  document.addEventListener('tizenhwkey', function(e)
  {
    if (e.keyName === "back")
    {
      try
      {
          tizen.application.getCurrentApplication().exit();
      }
      catch (ignore) {}
    }
  });
  displayTime();
  initDigitalWatch();
  bindEvents();
};
