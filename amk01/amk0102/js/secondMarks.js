createSecondMarks = function() {
// add a wrapper to the dom
	document.getElementById( 'rec_time' ).insertAdjacentHTML( 'beforeend', '<div id="markerSeconds"></div>' );
// start a loop to generate second marks on the watchface
	for( i = 1; i < 61; i++ ) {
		// define variables
			secondMarkAngle = ( 360 / 60 ) * i ;
			angleRotateDefinition = 'rotate(' + secondMarkAngle + 'deg)';
			angleRotateTargetWrapper = document.getElementById('markerSeconds');
			angleTargetId = 'markerSecond' + i ;
			angleRotateTarget = '<div id="' + angleTargetId + '" class="markerSecond"></div>';
		// define functions
			generateSecondMarkI = function() {
				angleRotateTargetWrapper.insertAdjacentHTML( 'beforeend', angleRotateTarget );
			};
			rotateSecondMarkI = function() {
				document.getElementById(angleTargetId).style.transform = angleRotateDefinition;
			};
		// run stuff
			generateSecondMarkI();
			rotateSecondMarkI();
	}
};
createSecondMarks();
