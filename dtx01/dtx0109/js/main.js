/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var flagConsole = false,
battery = navigator.battery || navigator.webkitBattery || navigator.mozBattery,
interval;

function displayWeekDay(date) {
	var str_day = document.getElementById('str_day'),
	get_day = date.getDay(),
	str_allday;
	arr_day = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
	arr_month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec" ], 
	get_date = date.getDate(),
	month = date.getMonth();

	if (get_date < 10) {
		get_date = "0" + get_date;
	}

	str_allday = arr_day[get_day] + " " + get_date + " " + arr_month[month];
	str_day.innerHTML = str_allday;
}

function displayTime() {

// NUMBERS TO WORDS FROM http://codereview.stackexchange.com/a/90563
	var ONE_TO_NINETEEN = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];

	var TENS = ["ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];

	var SCALES = ["thousand", "million", "billion", "trillion"];

	// helper function for use with Array.filter
	function isTruthy(item) {
		return !!item;
	}

	// convert a number into "chunks" of 0-999
	function chunk(number) {
		var thousands = [];

		while(number > 0) {
			thousands.push(number % 1000);
			number = Math.floor(number / 1000);
		}

		return thousands;
	}

	// translate a number from 1-999 into English
	function inEnglish(number) {
		var thousands, hundreds, tens, ones, words = [];

		if(number < 20) {
			return ONE_TO_NINETEEN[number - 1]; // may be undefined
		}

		if(number < 100) {
			ones = number % 10;
			tens = number / 10 | 0; // equivalent to Math.floor(number / 10)

			words.push(TENS[tens - 1]);
			words.push(inEnglish(ones));

			return words.filter(isTruthy).join("-");
		}

		hundreds = number / 100 | 0;
		words.push(inEnglish(hundreds));
		words.push("hundred");
		words.push(inEnglish(number % 100));

		return words.filter(isTruthy).join(" ");
	}

	// append the word for a scale. Made for use with Array.map
	function appendScale(chunk, exp) {
		var scale;
		if(!chunk) {
			return null;
		}
		scale = SCALES[exp - 1];
		return [chunk, scale].filter(isTruthy).join(" ");
	}

	var str_hours = document.getElementById('str_hours'),
	str_console = document.getElementById('str_console'),
	str_minutes = document.getElementById('str_minutes'),
	str_ampm = document.getElementById('str_ampm'),
	date;

	try{
		date  = tizen.time.getCurrentDateTime();
	}catch(e) {
		alert(e.message);
	}

	displayWeekDay(date);

	hours = date.getHours();
	if (hours == "0") {
		timeHours = "zero";
	} else {
		timeHours = inEnglish(hours);
	}
	str_hours.innerHTML = timeHours;
	minutes = date.getMinutes();
	if (minutes == "0") {
		timeMinutes = "zero";
	} else {
		timeMinutes = inEnglish(minutes);
	}
	str_minutes.innerHTML = timeMinutes;

	if (flagConsole) {
		str_console.style.visibility = 'visible';
		flagConsole = false;
	} else {
		str_console.style.visibility = 'hidden';
		flagConsole = true;
	}
}

function initDigitalWatch() {
	document.getElementsByTagName('body')[0].style.backgroundImage = "url('/images/bg.jpg')";
	interval = setInterval(displayTime, 500);
}

function ambientDigitalWatch() {
	clearInterval(interval);
	document.getElementsByTagName('body')[0].style.backgroundImage = "none";
	displayTime();
	document.getElementById('str_console').style.visibility = 'visible';
}

function getBatteryState() {
	var battery_level = Math.floor(battery.level * 10),
	battery_fill = document.getElementById('battery_fill');

	battery_level = battery_level;
	battery_fill.style.width = battery_level * 6 + "%";

}

function bindEvents() {
	battery.addEventListener('chargingchange', getBatteryState);
	battery.addEventListener('chargingtimechange', getBatteryState);
	battery.addEventListener('dischargingtimechange', getBatteryState);
	battery.addEventListener('levelchange', getBatteryState);

	// add eventListener for timetick
	window.addEventListener('timetick', function() {
		ambientDigitalWatch();
	});

	// add eventListener for ambientmodechanged
	window.addEventListener('ambientmodechanged', function(e) {
		console.log("ambientmodechanged : " + e.detail.ambientMode);
		if (e.detail.ambientMode === true) {
			// rendering ambient mode case
			ambientDigitalWatch();

		} else {
			// rendering normal case
			initDigitalWatch();
		}
	});
}

window.onload = function() {
	document.addEventListener('tizenhwkey', function(e) {
		if (e.keyName === "back") {
			try {
				tizen.application.getCurrentApplication().exit();
			} catch (ignore) {}
		}
	});

	displayTime();
	initDigitalWatch();
	bindEvents();
};
